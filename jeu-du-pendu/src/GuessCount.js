import PropTypes from 'prop-types'
import React from 'react'

import './GuessCount.css'

const GuessCount = ({ guesses, currentJoueur, currentCouleur }) =>
    <div className={`flex-container joueur-${currentJoueur + 1} ${currentCouleur}`} >
        <div className="item auto size-1">Numero d'essai : {guesses}</div>
        <div className="item auto size-1">Joueur Courant : <strong>{currentJoueur} </strong> </div>
    </div>
GuessCount.propTypes = {
    guesses: PropTypes.number.isRequired,
    currentJoueur: PropTypes.number.isRequired,
    currentCouleur: PropTypes.string.isRequired,
}
export default GuessCount
