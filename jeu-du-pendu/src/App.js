//import shuffle from 'lodash.shuffle'
//import update from 'react-addons-update';
import React, { Component } from 'react';
import './App.css';
import Letter from './Letter'
import GuessCount from './GuessCount'
import JeuFini from './JeuFini'
import Joueur from './Joueur'

const LETTERS = 'ABCDEFGHIJKLMNOPQRSTUVWXYZ'
const MOT = 'BONJOUR'
const DEFAULT_STATE = {
  guesses: 0,
  matchedLetterIndices: new Set(),
  matchedMotIndices: new Set(),
  currentJoueur: 1,
  jeuFini: 0,
  joueurGagner: 0,
  currentCouleur: 'bleu',
}
class App extends Component {
  resetJeu = index => {
    this.setState(DEFAULT_STATE) // Mieux !
    this.setState({ joueurs: this.generateJouers() }) // Mieux !
  }
  generateLetters() {
    return LETTERS.split("")
  }
  generateMot() {
    return MOT.split("")
  }
  generateJouers() {
    const joueurs = [
      { id: 1, score: 0, essai: 0, statut: true, color: 'bleu' },
      { id: 2, score: 0, essai: 0, statut: false, color: 'green' },
    ]
    return joueurs
  }

  state = {
    letters: this.generateLetters(),
    mot: this.generateMot(),
    guesses: 0,
    matchedLetterIndices: new Set(),
    matchedMotIndices: new Set(),
    joueurs: this.generateJouers(),
    currentJoueur: 1,
    jeuFini: 0,
    joueurGagner: 0,
    currentCouleur: 'bleu',
  }
  // Arrow fx for binding
  handleLetterMotClick = index => {
    console.log(this)
  }

  // Arrow fx for binding
  handleLetterClick = index => {

    const { guesses, mot, matchedLetterIndices, matchedMotIndices, currentJoueur, joueurs } = this.state

    const indexMatched = matchedLetterIndices.has(index)
    const motLength = mot.length
    const matchedMotIndicesLength = matchedMotIndices.size

    if (!indexMatched && motLength > matchedMotIndicesLength) {
      const newGuesses = guesses + 1
      this.setState({ guesses: newGuesses })

      this.setState({ matchedLetterIndices: this.state.matchedLetterIndices.add(index) })
      const currentLetter = LETTERS[index]
      var indexLetterMotExist = mot.indexOf(currentLetter)
      const indices = matchedMotIndices

      var currentJoueurArr = joueurs.filter(function (item) {
        return item.statut === true;
      });
      var nextJoueurArr = joueurs.filter(function (item) {
        return item.statut === false;
      });
      const indexCurrentJoueurArr = joueurs.indexOf(currentJoueurArr[0])
      const indexNextJoueurArr = joueurs.indexOf(nextJoueurArr[0])
      const newJoueurs = joueurs

      if (indexLetterMotExist === -1) {
        newJoueurs[indexCurrentJoueurArr].statut = false
        newJoueurs[indexNextJoueurArr].statut = true
        this.setState({ currentJoueur: newJoueurs[indexNextJoueurArr].id })
        this.setState({ currentCouleur: newJoueurs[indexNextJoueurArr].color })
      }

      // mettre a jour le score de joueur courant
      if (indexLetterMotExist !== -1) {
        newJoueurs[indexCurrentJoueurArr].score += 1
      }

      // ajouter l'index de la lettre trouve dans le mot dans la table "indexLetterMotExist"
      while (indexLetterMotExist !== -1) {
        indices.add(indexLetterMotExist);
        indexLetterMotExist = mot.indexOf(currentLetter, indexLetterMotExist + 1)
      }

      // mettre a jour le nombre d'essai pour le joueur courant
      newJoueurs[indexCurrentJoueurArr].essai += 1


      this.setState({ joueurs: newJoueurs })
      this.setState({ matchedMotIndices: indices })

      // jeu fini 
      if (matchedMotIndices.size === motLength) {
        if (joueurs[0]['score'] > joueurs[1]['score']) {
          this.setState({ joueurGagner: 1 })
        } else if (joueurs[0]['score'] < joueurs[1]['score']) {
          this.setState({ joueurGagner: 2 })
        } else if (joueurs[0]['score'] === joueurs[1]['score']) {

          if (joueurs[0]['essai'] > joueurs[1]['essai']) {
            this.setState({ joueurGagner: 1 })
          } else if (joueurs[0]['essai'] < joueurs[1]['essai']) {
            this.setState({ joueurGagner: 2 })
          } else if (joueurs[0]['essai'] === joueurs[1]['essai']) {
            this.setState({ joueurGagner: 0 })
          }
        }

        this.setState({ jeuFini: 1 })
      }
    }
  }

  getFeedbackForLetter(index) {
    //return  'visible'
    const { mot, matchedLetterIndices } = this.state
    const indexMatched = matchedLetterIndices.has(index)
    const currentLetter = LETTERS[index]
    const indexLetterExist = mot.includes(currentLetter)
    if (indexLetterExist && indexMatched) {
      return 'justMismatched'
    }
    return indexMatched ? 'justMatched' : 'visible'
  }
  getFeedbackForMot(index) {
    //return  'visible'
    //console.log(this.state.mot)
    const { matchedMotIndices } = this.state
    const indexMatched = matchedMotIndices.has(index)
    if (matchedMotIndices) {
      if (indexMatched) {
        return 'visible'
      }
    }
    return 'hidden'
  }

  render() {
    const { letters, mot, guesses, joueurs, currentJoueur, currentCouleur, jeuFini, joueurGagner } = this.state

    return (
      <div className="App">
        <GuessCount guesses={guesses} currentJoueur={currentJoueur} currentCouleur={currentCouleur} />
        <div className="letters mot">
          {mot.map((letter, index) => (
            <Letter
              letter={letter}
              key={index}
              feedback={this.getFeedbackForMot(index)}
              index={index}
              onClick={this.handleLetterMotClick}
            />
          ))}
        </div>
        <hr />
        <div className="flex-container joueurs">
          {joueurs.map((joueur, index) => (
            <Joueur
              id={joueur.id}
              key={index}
              score={joueur.score}
              essai={joueur.essai}
              color={joueur.color}
              index={index}
            />
          ))}

        </div>
        <hr />
        <div className="letters">
          {letters.map((letter, index) => (
            <Letter
              letter={letter}
              feedback={this.getFeedbackForLetter(index)}
              key={index}
              index={index}
              onClick={this.handleLetterClick}
            />
          ))}
        </div>
        <JeuFini jeuFini={jeuFini} joueurGagner={joueurGagner} />
        <hr />
        <div className="text-center">
          <button className="btn btn-primary"
            onClick={this.resetJeu}
            type="button">Reset Jeu</button>
        </div>
      </div >
    )
  }
}

export default App
