import PropTypes from 'prop-types'
import React from 'react'

import './JeuFini.css'

const JeuFini = ({ jeuFini, joueurGagner }) => <h3 className="text-center">{jeuFini === 1 && (joueurGagner === 0 ? "Score et nombre d'essai sont à égalité " : `Joueur numero ${joueurGagner} gagner  !`)}</h3>

JeuFini.propTypes = {
    jeuFini: PropTypes.number.isRequired,
    joueurGagner: PropTypes.number.isRequired,
}
export default JeuFini
