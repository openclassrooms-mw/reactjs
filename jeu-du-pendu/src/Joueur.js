import PropTypes from 'prop-types'
import React from 'react'

import './Joueur.css'

const Joueur = ({ id, score, essai, color }) =>
    // <span className={id % 1 ? "item item-joueur-0 auto joueur" : "item item-joueur-1 auto joueur"}>
    <span className={`item item-joueur-${id} ${color} auto joueur`}>
        <span className="flex-container-simpl">
            <span className="auto">Joueur : {id}</span>
            <span className="auto">Score : {score}</span>
            <span className="auto">Essai : {essai}</span>
        </span>
    </span>
Joueur.propTypes = {
    id: PropTypes.number.isRequired,
    score: PropTypes.number.isRequired,
    essai: PropTypes.number.isRequired,
}
export default Joueur
